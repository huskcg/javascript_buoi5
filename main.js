//Ex1
function calcExcercise1() {
  var diem_chuan = document.getElementById("diem-chuan").value * 1;
  var khu_vuc = document.getElementById("khu-vuc").value * 1;
  var doi_tuong = document.getElementById("doi-tuong").value * 1;
  var number1 = document.getElementById("number1").value * 1;
  var number2 = document.getElementById("number2").value * 1;
  var number3 = document.getElementById("number3").value * 1;

  if (diem_chuan > 0) {
    if (number1 > 0 && number2 > 0 && number3 > 0) {
      var total = khu_vuc + doi_tuong + number1 + number2 + number3;
      if (total >= diem_chuan) {
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Bạn đã đậu. Tổng điểm: ${total}`;
      } else {
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Bạn đã rớt. Tổng điểm: ${total}`;
      }
    } else {
      document.getElementById(
        "result-ex1"
      ).innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`;
    }
  } else {
    document.getElementById(
      "result-ex1"
    ).innerHTML = `Điểm chuẩn phải lớn hơn 0`;
  }
}
//Ex2
function calcExcercise2() {
  var username = document.getElementById("username").value;
  var kw = document.getElementById("kw").value * 1;
  var result;
  if (kw > 0) {
    if (kw <= 50) {
      result = 500 * kw;
      document.getElementById(
        "result-ex2"
      ).innerHTML = `Họ tên: ${username}; Tiền điện: ${result.toLocaleString()}`;
    } else if (kw > 50 && kw <= 100) {
      result = 25000 + (kw - 50) * 650;
      document.getElementById(
        "result-ex2"
      ).innerHTML = `Họ tên: ${username}; Tiền điện: ${result.toLocaleString()}`;
    } else if (kw > 100 && kw <= 200) {
      result = 57500 + (kw - 100) * 850;
      document.getElementById(
        "result-ex2"
      ).innerHTML = `Họ tên: ${username}; Tiền điện: ${result.toLocaleString()}`;
    } else if (kw > 200 && kw <= 350) {
      result = 142500 + (kw - 200) * 1100;
      document.getElementById(
        "result-ex2"
      ).innerHTML = `Họ tên: ${username}; Tiền điện: ${result.toLocaleString()}`;
    } else {
      result = 307500 + (kw - 350) * 1300;
      document.getElementById(
        "result-ex2"
      ).innerHTML = `Họ tên: ${username}; Tiền điện: ${result.toLocaleString()}`;
    }
  } else {
    return alert("Số kw không hợp lệ, số kw phải > 0. Vui lòng nhập lại");
  }
}
